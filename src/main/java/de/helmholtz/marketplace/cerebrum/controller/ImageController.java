package de.helmholtz.marketplace.cerebrum.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.SneakyThrows;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.Min;
import java.util.Base64;
import java.util.List;

import de.helmholtz.marketplace.cerebrum.entity.Image;
import de.helmholtz.marketplace.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.marketplace.cerebrum.service.ImageService;
import de.helmholtz.marketplace.cerebrum.utils.CerebrumControllerUtilities;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,
        path = "${spring.data.rest.base-path}/images")
@Tag(name = "images", description = "The Image API")
public class ImageController
{
    private final ImageService imageService;

    public ImageController(ImageService imageService)
    {
        this.imageService = imageService;
    }

    /* get Images */
    @Operation(summary = "get array list of all images")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(array = @ArraySchema(
                            schema = @Schema(implementation = Image.class)))),
            @ApiResponse(responseCode = "400", description = "invalid request",
                    content = @Content(array = @ArraySchema(
                            schema = @Schema(implementation = CerebrumApiError.class))))
    })
    @GetMapping(path = "")
    public Iterable<Image> getImages(
            @Parameter(description = "specify the name of the image to search for")
            @RequestParam(value = "name", defaultValue = "") String name,
            @Parameter(description = "specify the page number")
            @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @Parameter(description = "limit the number of records returned in one page")
            @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
            @Parameter(description = "sort the fetched data in either ascending (asc) " +
                    "or descending (desc) according to one or more of the images " +
                    "properties. Eg. to sort the list in ascending order base on the " +
                    "name property; the value will be set to name.asc")
            @RequestParam(value = "sort", defaultValue = "name.asc") List<String> sorts)
    {
        return (name == null || name.isEmpty()) ?
                imageService.getImages(PageRequest.of(page, size,
                        Sort.by(CerebrumControllerUtilities.getOrders(sorts)))) :
                imageService.getImages(name, PageRequest.of(page, size,
                        Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
    }


    /* get Image */
    @Operation(summary = "find image by ID",
            description = "Returns a detailed image information " +
                    "corresponding to the ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Image.class))),
            @ApiResponse(responseCode = "400", description = "invalid image ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "404", description = "image not found",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class)))
    })
    @GetMapping(path = "/{uuid}")
    public Image getImage(
            @Parameter(description = "ID of the image that needs to be fetched")
            @PathVariable(name = "uuid") String uuid)
    {
        return imageService.getImage(uuid);
    }

    /* create image */
    @SneakyThrows
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "add a new image",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "image created",
                    content = @Content(schema = @Schema(implementation = Image.class))),
            @ApiResponse(responseCode = "400", description = "invalid ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @PostMapping("")
    public ResponseEntity<Image> createImage(
            @Parameter(description = "name of the image")
            @RequestParam("name") String name,
            @RequestParam("image") MultipartFile image,
            UriComponentsBuilder uriComponentsBuilder)
    {
        Image img = new Image();
        img.setName(name);
        img.setImage(
                Base64.getEncoder().encodeToString(new Binary(BsonBinarySubType.BINARY, image.getBytes()).getData()));
        return imageService.createImage(img, uriComponentsBuilder);
    }

    /* update image */
    @SneakyThrows
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "update an existing image",
            description = "Update part (or all) of an image information",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Image.class))),
            @ApiResponse(responseCode = "201", description = "image created",
                    content = @Content(schema = @Schema(implementation = Image.class))),
            @ApiResponse(responseCode = "400", description = "invalid ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @PutMapping(path = "/{uuid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Image> updateImage(
            @Parameter(description = "Unique identifier of the image that needs to be updated")
            @PathVariable(name = "uuid") String uuid,
            @Parameter(description = "name of the image")
            @RequestParam("name") String name,
            @Parameter(description = "")
            @RequestParam("image") MultipartFile image, UriComponentsBuilder uriComponentsBuilder)
    {
        Image img = new Image();
        img.setName(name);
        img.setImage(Base64.getEncoder().encodeToString(
                new Binary(BsonBinarySubType.BINARY, image.getBytes()).getData()));

        return imageService.updateImage(uuid, img, uriComponentsBuilder);
    }

    /* delete Image */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "deletes an image",
            description = "Removes the record of the specified " +
                    "image id from the database. The image " +
                    "unique identification number cannot be null or empty",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "successful operation", content = @Content()),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @DeleteMapping(path = "/{uuid}")
    public ResponseEntity<Image> deleteImage(
            @Parameter(description="Image id to delete", required=true)
            @PathVariable(name = "uuid") String uuid)
    {
        return imageService.deleteImage(uuid);
    }
}
