package de.helmholtz.marketplace.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

import de.helmholtz.marketplace.cerebrum.entity.MarketService;
import de.helmholtz.marketplace.cerebrum.repository.fragment.CerebrumRepository;

public interface MarketServiceRepository extends MongoRepository<MarketService, String>, CerebrumRepository<MarketService>
{
    Optional<MarketService> findByUuid(String uuid);

    Optional<MarketService> findByName(@Param("name") String name);

    Optional<MarketService> findByEntryPoint(@Param("entryPoint") String url);

    @Query(value = "{'serviceProviders.$id' : ?0 }", fields = "{'serviceProviders' : 0}")
    Page<MarketService> findByServiceProvidersUsingUuid(String uuid, PageRequest pageRequest);

    @SuppressWarnings("UnusedReturnValue")
    Optional<MarketService> deleteByUuid(String id);
}
