package de.helmholtz.marketplace.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

import de.helmholtz.marketplace.cerebrum.annotation.ForeignKey;
import de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator;

import static de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator.generate;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class Software extends AuditMetadata
{
    @Schema(description = "Unique identifier of the software.",
            example = "sfw-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("sfw");

    @NotNull
    @Schema(description = "Name of a software", example = "NextCloud", required = true)
    private String name;

    private String description;

    @Schema(description = "", example = "pht-01eac6d7-0d35-1812-a3ed-24aec4231940")
    @ForeignKey
    private String logoId;

    public void setUuid(@Nullable String uuid)
    {
        this.uuid =  Boolean.TRUE.equals(
                CerebrumEntityUuidGenerator.isValid(uuid))
                ? uuid : generate("sfw");
    }
}
