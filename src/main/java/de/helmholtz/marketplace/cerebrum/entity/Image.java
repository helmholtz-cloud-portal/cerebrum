package de.helmholtz.marketplace.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator;
import static de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator.generate;
import static de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator.isValid;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class Image extends AuditMetadata
{
    @Schema(description = "Unique identifier of the market service.",
            example = "img-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("img");

    @NonNull
    private String name;

    @NonNull
    private String image;

    public void setUuid(@Nullable String uuid)
    {
        this.uuid =  Boolean.TRUE.equals(
            CerebrumEntityUuidGenerator.isValid(uuid))
            ? uuid : generate("img");
    }
}
