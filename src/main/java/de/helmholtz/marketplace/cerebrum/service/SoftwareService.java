package de.helmholtz.marketplace.cerebrum.service;

import com.github.fge.jsonpatch.JsonPatch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.marketplace.cerebrum.entity.Software;
import de.helmholtz.marketplace.cerebrum.repository.SoftwareRepository;
import de.helmholtz.marketplace.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.marketplace.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class SoftwareService extends CerebrumServiceBase<Software, SoftwareRepository, ForeignKeyExecutorService>
{
    private final SoftwareRepository softwareRepository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected SoftwareService(SoftwareRepository repository,
                              ForeignKeyExecutorService foreignKeyExecutorService)
    {
        super(Software.class, SoftwareRepository.class, ForeignKeyExecutorService.class);
        this.softwareRepository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<Software> getSoftware(PageRequest page)
    {
        return getAllEntities(page, softwareRepository);
    }

    public Software getSoftware(String uuid)
    {
        return getEntity(uuid, softwareRepository);
    }

    public Page<Software> getSoftware(String name, PageRequest page)
    {
        return softwareRepository.findByName(name, page);
    }

    public Software createSoftware(Software entity)
    {
        return createEntity(entity, softwareRepository, foreignKeyExecutorService);
    }

    public ResponseEntity<Software> createSoftware(Software entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return createEntity(entity, softwareRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Software> updateSoftware(
            String uuid, Software entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return updateEntity(uuid, entity, softwareRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Software> partiallyUpdateSoftware(String uuid, JsonPatch patch)
    {
        return partiallyUpdateEntity(uuid, softwareRepository, foreignKeyExecutorService, patch);
    }

    public ResponseEntity<Software> deleteSoftware(String uuid)
    {
        return deleteEntity(uuid, softwareRepository, foreignKeyExecutorService);
    }
}
